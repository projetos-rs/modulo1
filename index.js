const express = require('express');

const server = express();

server.use(express.json());

const users = ['Diego', 'Rogério', 'Victor'];

//MIDDLEWARES GLOBAIS
server.use((req, res, next) => {
  console.time('Tempo');
  console.log(`Requisição! \nMétodo: ${req.method} \nURL: ${req.url}`);
  next();
  console.timeEnd('Tempo');
  
})

//MIDDLEWARES LOCAIS
function checkUser(req, res, next){
  if(!req.body.name){
    return res.status(400).json({ error: 'User name is required' });
  }

  return next();
}

function checkIndex(req, res, next){
  const user = users[req.params.index];

  if(!users){
    return res.status(404).json({ error: 'User does not exist!' });
  }

  req.user = user;

  return next();
}


//RETORNA TODOS OS USUÁRIOS
server.get('/users', (req, res) => {
  return res.json(users);
})

//RETORNA UM USUÁRIO
server.get('/users/:index', checkIndex, (req, res) => {
  const { index } = req.params;

  return res.json({message: req.user });

})

//CRIA UM USUÁRIO
server.post('/users/', checkUser, (req, res) => {
  const { name } = req.body
  users.push(name)

  console.log(`${name} foi cadastrado com sucesso`)
  return res.json(users)
})

//EDITAR USUÁRIO
server.put('/users/:index', checkUser, checkIndex, (req, res) =>{
  const { index } = req.params;
  const { name } = req.body;

  users[index] = name;

  return res.json(users);
})

//DELETAR USUÁRIO
server.delete('/users/:index', checkIndex, (req, res) => {
  const { index } = req.params;

  users.splice(index, 1)
  return res.json(users)
})

server.listen(3000);